package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/queue"
)

func main() {
	var data [][]string
	baseURL := "https://www.lamoda.ru/c/15/shoes-women/?is_new=1&ad_id=917210&page="
	c := colly.NewCollector(
		colly.AllowedDomains("www.lamoda.ru"),
		colly.UserAgent("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"),
	)

	c.Limit(&colly.LimitRule{
		DomainGlob:  "*lamoda.ru*",
		Parallelism: 2,
		RandomDelay: 1 * time.Second,
	})

	var foundItems bool

	c.OnHTML("div.x-product-card__card", func(e *colly.HTMLElement) {
		foundItems = true
		title := e.ChildText("div.x-product-card-description__product-name")
		price := e.ChildText("span.x-product-card-description__price-single")

		if title != "" && price != "" {
			fmt.Printf("Product: %s, Price: %s\n", title, price)
			data = append(data, []string{title, price})
		}
	})

	c.OnError(func(r *colly.Response, err error) {
		fmt.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
	})

	q, _ := queue.New(
		2,
		&queue.InMemoryQueueStorage{MaxSize: 10000},
	)

	page := 1
	for {
		foundItems = false
		pageURL := baseURL + strconv.Itoa(page)
		q.AddURL(pageURL)

		if !foundItems {
			break
		}
		page++
	}

	q.Run(c)

	file, err := os.Create("output.csv")
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range data {
		err := writer.Write(value)
		if err != nil {
			log.Fatal("Cannot write to file", err)
		}
	}
}
